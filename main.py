import time


def timerfunc(func):
    """
    Декоратор для определения времени работы функции
    """

    def function_timer(*args, **kwargs):
        """
        врапер
        """
        start = time.time()
        value = func(*args, **kwargs)
        end = time.time()
        runtime = end - start
        print(f"Время работы функции {func.__name__} : {runtime:.4f} секунд")
        return value

    return function_timer


@timerfunc
def generate_data():
    import secrets
    import string
    import random
    from numpy import random as num_rand
    random_list = [num_rand.randint(-100000000, 10000000000 + 1) for i in range(1, 1000)]
    random_list_of_string = [''.join(random.choices(string.ascii_uppercase + string.digits, k=10)) for i in range(1, 1024)]
    random_d = {}
    for i in range(1, 10000):
        random_d.update({secrets.token_urlsafe(): num_rand.randint(-10000, 10000000000 + 1)})
    random_d.update({'random_list': random_list})
    random_d.update({'random_list_of_strings': random_list_of_string})

    return random_d


@timerfunc
def save_json(_data):
    import json
    json_file = open('data.json', 'wt')
    json_file.write(json.dumps(_data))
    json_file.close()


@timerfunc
def save_yaml(_data):
    from yaml import dump
    try:
        from yaml import CLoader as Loader, CDumper as Dumper, CSafeLoader as SafeLoader
    except ImportError:
        from yaml import Loader, Dumper, SafeLoader
    output = dump(_data, Dumper=Dumper)
    yaml_file = open('data.yaml', 'w')
    yaml_file.write(output)
    yaml_file.close()


@timerfunc
def save_pickle(_data):
    import pickle
    pickle_file = open('data.pickle', 'wb')
    pickle_file.write(pickle.dumps(_data))
    pickle_file.close()


@timerfunc
def load_json():
    import json
    json_file = open('data.json', 'rt')
    _data = json.loads(json_file.read())
    json_file.close()
    return _data


@timerfunc
def load_yaml():
    from yaml import load
    try:
        from yaml import CLoader as Loader, CDumper as Dumper, CSafeLoader as SafeLoader
    except ImportError:
        from yaml import Loader, Dumper, SafeLoader
    yaml_file = open('data.yaml', 'r')
    _data = load(yaml_file, Loader=SafeLoader)
    yaml_file.close()
    return _data


@timerfunc
def load_pickle():
    import pickle
    pickle_file = open('data.pickle', 'rb')
    _data = pickle.loads(pickle_file.read())
    pickle_file.close()
    return _data


def print_size_files():
    import os
    print(f"Размер дампа json: {os.path.getsize('data.json')}b")
    print(f"Размер дампа yaml: {os.path.getsize('data.yaml')}b")
    print(f"Размер дампа pickle: {os.path.getsize('data.pickle')}b")


@timerfunc
def start():
    from pympler import asizeof
    data = generate_data()
    print(f"обьем сгенерированных данных: {asizeof.asizeof(data)}b")
    save_json(data)
    save_yaml(data)
    save_pickle(data)
    print_size_files()
    load_json()
    load_pickle()
    load_yaml()


if __name__ == '__main__':
    start()